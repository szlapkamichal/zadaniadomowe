import java.util.Scanner;

public class Zadanie2_4 {
    // Wczytywać od użytkownika liczby tak długo aż nie pojawi się cyfra 0
    // Zsumować wszystkie liczby

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podawaj losowe liczby");
        int suma = 0;
        for (int liczba = scanner.nextInt(); liczba != 0; liczba = scanner.nextInt()){
        suma += liczba;
        }
        System.out.println("Napisano liczbę 0. Kończy to program.");
        System.out.println("Suma liczb to: " + suma);

    }
}
