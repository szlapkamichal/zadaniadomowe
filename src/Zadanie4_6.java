import java.util.Scanner;

public class Zadanie4_6 {
    // Szyf Cezara
    // Użytkownik podaje ciąg znaków do zaszyfrowania
    // Podaje przesunięcie
    //Program zwraca znaki po zaszyfrowaniu

    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj napis do zaszyfrowania");
        String napis = scanner.nextLine();
        System.out.println("Podaj liczbę o jaką zostanie zaszyfrowany tekst");
        int liczba = scanner.nextInt();

        String szyfr = "";
        for (int i = 0; i < napis.length(); i++){
            char znak = napis.charAt(i);
            znak = (char) (znak + liczba);
            System.out.print(znak);


        }
    }
}
