import java.util.Scanner;

public class Zadanie1_4 {
    //Obliczanie podatku dochodowego

    public static void main(String[] args) {

        System.out.println("Podaj swój roczny dochod w zł");
        Scanner scanner = new Scanner(System.in);
        double dochod = scanner.nextDouble();

        //obliczanie podatku w zależności od progu

        if (dochod < 85528){
            double prog1 = (dochod * 0.18) - 556.02;
            System.out.println("Jesteś w I progu podatkowym");
            System.out.println("Twój roczny podatek dochodowy to: " + prog1 + "zł");
        } else {
            double prog2 = 14839.02 + (0.32 * (dochod - 85528));
            System.out.println("Jesteś w II progu podatkowym");
            System.out.println("Twój roczny podatek dochodowy to: " + prog2 + "zł");
        }
    }
}
