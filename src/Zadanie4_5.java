import java.util.Scanner;

public class Zadanie4_5 {
    // napisać program do weryfikacji sparowania nawiasów w wyrażeniu pisanym ciągiem
    // program będzie sprawdzał tylko ilość nawiasów otwierających i porównywał je z ilośćią zamykających
    // Przakładowe działanie: (2 * (3.4 - (-7)/2)*(a-2)/(b-1))
    public static void main (String[] args){

        Scanner scanner = new Scanner(System.in);
        System.out.println("Napisz działanie");
        String wyrazenie = scanner.nextLine();


        int sumaOtwierajacych = 0;
        int sumaZamykajacych = 0;
        for (int i = 0; i < wyrazenie.length(); i++){
            if (wyrazenie.charAt(i) == '(') {
                sumaOtwierajacych = sumaOtwierajacych + 1;
            } else if (wyrazenie.charAt(i) == ')'){
                sumaZamykajacych = sumaZamykajacych + 1;
            }
        }
        if (sumaOtwierajacych == sumaZamykajacych) {
            System.out.println("OK - nawiasy sa w porządku");
        } else {
            System.out.println("Błędne sparowanie nawiasów");
        }
    }
}
