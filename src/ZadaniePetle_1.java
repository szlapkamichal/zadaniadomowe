public class ZadaniePetle_1 {
    // Napisać program wypisujący na ekranie wszystkie liczby całkowite od 1 do 100
    // - for
    // - while

    public static void main (String[] args){
        System.out.print("Liczby od 1 do 100 z pętli for: ");
        for(int i = 1; i <= 100; i++ ){
            System.out.print(i + ", ");
        }
        System.out.println();;
        System.out.print("Liczby od 1 do 100 z pętli while: ");
        int x = 0;
        while (x < 100){

            x++;

            System.out.print(x + ", ");
        }
    }

}
