import jdk.nashorn.internal.ir.IfNode;

import java.util.Scanner;

public class Zadanie1_2 {
    //Wczytać od użytkownika 3 liczby całkowite i wskazać największą z nich

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj trzy liczby całkowite");

        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        if (a > b && a > c){
            System.out.println(a);
        } else if (b > c && b > a) {
            System.out.println(b);
        } else {
            System.out.println(c);
        }

    }
}
