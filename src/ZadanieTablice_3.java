import java.util.Random;
import java.util.Scanner;

public class ZadanieTablice_3 {
    /*
    Napisz program losujący 30 elementów w tablicy.
    Program ma zapytać użytkownika o liczbę i dać odpowiedź, czy podana liczba znajduje się w tablicy.
     */

    private static Random random = new Random();
    private static Scanner scanner = new Scanner(System.in);

    public static void losowanie (int n) {
        int[] tablica = new int[30];
        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = random.nextInt(30);
            System.out.print(tablica[i] + ", ");
        }
        System.out.println();
        System.out.println("Program wylosował 30 pseudo-losowych liczb.");
        System.out.println("Podaj swoją liczbę");

        int liczba = scanner.nextInt();
        int wynik = 0;
        for (int i = 0; i < tablica.length; i++){
            if (liczba == tablica[i]){
                wynik = wynik + 1;
            }
        }
        if (wynik > 1){
            System.out.println("BRAWO, trafifiłeś w wylosowaną liczbę " + wynik + " razy");
        } else if ( wynik > 0){
            System.out.println("Brawo, trafiłeś w wylosowaną liczbę jeden raz");
        } else {
            System.out.println("Niestety nie trafiłeś");
        }
    }

    public static void main(String[] args) {

    losowanie(1);
    }
}
