import java.util.Scanner;

public class ZadaniePetle_7 {
    // wyświetlić na ekranie liczby z przedziału od 100 do 300
    //korzystając z operatora trójargumentowego wypisać czy dana liczba jest parzysta czy nie




    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wszystkie liczby z przedziału {100;300}");
        for (int i = 100; i<=300; i++){
            System.out.print(i + ", ");
        }
        System.out.println();
        System.out.println("Podaj liczbę");
        int liczba = scanner.nextInt();

        if (liczba > 100 && liczba < 300) {
            if (liczba % 2 == 0) {
                System.out.println("Podana liczba jest liczbą parzystą");
            } else {
                System.out.println("Podana liczba jest nieprzarzysta");
            }
        } else {
            System.out.println("Podana liczba nie zawiera się w przedziale {100:300}");
        }


    }
}
