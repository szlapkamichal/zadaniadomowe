import java.util.Scanner;

public class Zadanie1_5 {
    //Program do obliczania wysokości raty w zależności od kwoty oraz ilosci rat

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj cenę towaru");
        double cena = scanner.nextDouble();

        if (cena < 100 || cena > 10000){
            System.out.println("Podana cena nie mieści w przedziale od 100zł do 10 000zł");
            System.out.println("Uruchom program ponownie i wstaw prawidłową wartość");
        } else {
            System.out.println("Podaj liczbę rat");
            int raty = scanner.nextInt();
            if (raty < 6 || raty > 48){
                System.out.println("Podana liczba rat nie zawiera się w przedziale między 6 a 48");
                System.out.println("Uruchom ponownie program i wstaw prawidłową wartość");
            } else if (raty >= 6 && raty <= 12) {
                double rataMiesieczna1 = (cena / raty) + (cena * 0.025);
                System.out.printf("Miesięczna rata: " + rataMiesieczna1);
            } else if (raty >=13 && raty <= 24) {
                double rataMiesięczna2 = (cena / raty) + (cena * 0.05);
                System.out.println("Misięczna rata: " + rataMiesięczna2);
            } else if (raty >= 25 && raty <= 48) {
                double rataMiesięczna3 = (cena / raty) + (cena * 0.1);
                System.out.println("Miesięczna rata: " + rataMiesięczna3);
            }
        }

    }
}
