import java.util.Scanner;

public class Zadanie4_2 {
    //pobrac od użytkownika ciag znaków
    //odwócić ten ciąg znaków
    //wyświetlić go na ekranie

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ciąg znaków");
        String napis = scanner.next();

        int ileZnakow = napis.length();


        for (int i = ileZnakow -1; i >= 0; i--) {

            char napisOdTylu = napis.charAt(i);
            System.out.print(napisOdTylu);
        }
    }


}
