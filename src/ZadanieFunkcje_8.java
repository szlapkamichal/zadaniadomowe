import java.util.Scanner;

public class ZadanieFunkcje_8 {
    //napisz program który sprawdza czy podana liczba jest liczbą pierwszą

    public static Scanner scanner = new Scanner(System.in);

    public static String pierwsza(int liczba) {
        int wynik = 0;
        for (int i = 1; i <= liczba; i++) {
            if (liczba % i == 0) {
                wynik = wynik + 1;
            }
        }
        if (wynik <= 2) {
            return "Jest to liczba pierwsza";
        } else {
            return "Nie jest to liczba pierwsza";
        }
    }
    public static void main(String[] args) {

        System.out.println("Podaj liczbę");
        int li = scanner.nextInt();
        System.out.println(pierwsza(li));
    }
}