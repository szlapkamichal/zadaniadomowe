import java.util.Random;
import java.util.Scanner;

public class ZadanieTablice_5 {
    // Napisz program losujący 30 elementów w tablicy.
    // Program ma zapytać użytkownika o liczbę i dać odpowiedz, czy podana liczba znajduje się w tablicy.

    public static Random random = new Random();
    public static Scanner scanner = new Scanner(System.in);

    public static int[] TworzTablice() {
        int[] tablica = new int[80];
        for (int i = 0; i < tablica.length; i++ ){
            tablica[i] = random.nextInt(99);
        }
        return tablica;
    }

    public static void main(String[] args) {

        TworzTablice();
        System.out.println("Podaj swoją liczbę - zakres 0 - 100");
        int liczba = scanner.nextInt();

        int wynik = 0;
        for (int i = 0; i < TworzTablice().length; i++ ) {
            if (TworzTablice()[i] == liczba) {
                wynik++;
            }
        }
            if (wynik > 0){
                System.out.println("Brawo - podana liczba znajduje się w tablicy");

        }

    }


}
