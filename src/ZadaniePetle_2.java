public class ZadaniePetle_2 {
    //wyświetlić wszystkie liczby podzielne przez 7
    // zakres liczb 1 - 500

    public static void main(String[] args) {

        System.out.print("Wszystkie liczby z zakresu 1-500 podzielne przez 7 to: ");

        for (int i = 1; i < 500; i++) {

            if (i % 7 == 0) {

                System.out.print(i + ", ");
            }
        }
    }
}
