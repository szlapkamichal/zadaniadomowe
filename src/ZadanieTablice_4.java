import java.util.Scanner;

public class ZadanieTablice_4 {
    /*
    Napisz program  wczytujący z klawiatury n liczb całkowitych.
    Program ma znaleźć największą spośród podanych liczb oraz wydrukować na ekranie informację mówiącą o tym ile razy największa liczba wystąpiłą w podanym ciągu liczb.
     */

    private static Scanner scanner = new Scanner(System.in);

    private static void liczbyUzytkownika () {
        System.out.println("Na ilu liczbach chcesz pracować??");
        int liczba = scanner.nextInt();
        System.out.println("Podaj swoje liczby");
        int[] tablica = new int[liczba];
        for (int i = 0; i < liczba; i++) {
            tablica[i] = scanner.nextInt();
        }
        int max = tablica[0];
        int ileMax = 0;
        for (int i = 0; i < tablica.length; i++){
            if (tablica[i] > max){
                max = tablica[i];
            }
        }
        System.out.println("Największą liczbą w zbiorze jest: " + max);

        for (int i = 0; i < tablica.length; i++){
            if (tablica[i] == max){
                ileMax++;
            }
        }
        System.out.println("Występuje ona " + ileMax + " razy");
    }


    public static void main(String[] args) {

        liczbyUzytkownika();
    }
}
