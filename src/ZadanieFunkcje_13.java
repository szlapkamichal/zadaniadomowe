import java.util.Random;
import java.util.Scanner;

public class ZadanieFunkcje_13 {
    /*
    Napisz program sprawdzajacy umiejętność mnożenia użytkownika.
    Wylosuj dwie liczby oraz poproś użytkownika o podanie poprawnego wyniku działania.
    Program ma pytać, aż użytkownik nie poda poprawnej odpowiedzi
    */

    private static Scanner scanner = new Scanner(System.in);
    private static Random random = new Random();

    private static void sprawdzanieWyniku(int liczba1, int liczba2) {

        int wynik = scanner.nextInt();
        if (wynik == liczba1 * liczba2) {
            System.out.println("Brawo, wynik jest prawidłowy");
        } else {
            System.out.println("Źle, próbuj dalej");
            sprawdzanieWyniku(liczba1, liczba2);
        }
    }

    public static void main(String[] args) {

        int liczba1 = random.nextInt(99);
        int liczba2 = random.nextInt(99);

        System.out.println("---------MAŁY TEST---------");
        System.out.println("Podaj wynik mnożenia liczb: " + liczba1 + " oraz " + liczba2);

        sprawdzanieWyniku(liczba1, liczba2);
    }
}
