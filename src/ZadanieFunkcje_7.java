import java.util.Scanner;

public class ZadanieFunkcje_7 {
    /* napisać 3 funkcje:

    -wypisującą liczby podzielne przez 5 z zakresu m-n (m, n wczytywane od użytkownika)
    -funkcję dodającą dwie liczby naturnalne i zwracajacą sumę tych liczb
     */

    public static void wypiszLiczbyPodzielnePrzezPieć (int x, int y) {
        for (int i = x; i < y; i++) {
            if (i % 5 == 0) {
                System.out.print(i + ", ");
            }
        }
    }
    public static int dodajDoSiebieLiczby (int x, int y) {

        return (x + y);
    }


    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj zakres liczb");
        System.out.println("Liczby od");
        int m = scanner.nextInt();
        System.out.println("Liczby do:");
        int n = scanner.nextInt();
        System.out.println();
        System.out.println("Wszystkie liczby podziele przez 5 z tego przedziału to:");

        wypiszLiczbyPodzielnePrzezPieć(m,n);
        System.out.println();
        System.out.println(dodajDoSiebieLiczby(m,n));

    }

}
