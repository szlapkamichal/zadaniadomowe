import java.util.Random;

public class Zadanie3_2 {
    // Utworzyć tablicę 20 elementową z liczb całokwitych z przedziału 1 - 10
    //  Program ma wypisać natępnie ile razy każda z liczby występuje w ciągu

    public static void main(String[] args) {
        Random random = new Random();
        int[] tablica = new int[20];


        for (int i = 0; i < tablica.length; i++){
            tablica[i] = random.nextInt(10)+1;

        }
        for (int i = 0; i < tablica.length; i++){
            System.out.print(" " + tablica[i]);
        }
        int[] wystapienia = new int[11];
        System.out.println(" ");
        System.out.println("Wystąpienia: ");

        for (int i = 0; i < tablica.length; i++){
            wystapienia[tablica[i]]++;
        }

        for (int i = 1; i<wystapienia.length; i++){

            System.out.println(i + "-> " + wystapienia[i]);
        }

    }
}
