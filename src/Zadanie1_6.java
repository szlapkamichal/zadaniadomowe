import java.util.Scanner;

public class Zadanie1_6 {
    // Prosty kalkulator

    public static void main(String[] args) {
        System.out.println("Prosty kalkulator na liczbach całkowitych");
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj pierwszą liczbę");

        double a = scanner.nextDouble();

        System.out.println("Działanie");

        char działanie = scanner.next().charAt(0);

        System.out.println("Podaj drugą liczbę");

        double b = scanner.nextDouble();

        double wynik = 0;

        boolean sukces = true;
        switch (działanie) {
            case '+':
                wynik = a + b;
                break;
            case '-':
                wynik = a - b;
                break;
            case '*':
                wynik = a * b;
                break;
            case '/':
                if (b == 0){
                    sukces = false;
                } else {
                wynik = a / b;
                }
                break;
            default:
                System.out.println("Podane działanie jest nierozpoznane. Rozpocznij ponownie");

            }
            if (sukces == true){
                System.out.println(a + " " + działanie + " " + b + " =" + wynik);
            } else {
                System.out.println("nie dzielę przez 0");
            }

    }
}
