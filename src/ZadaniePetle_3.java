public class ZadaniePetle_3 {
    //program który sumuje wszystkie liczby od 1 do 100

    public static void main (String[] args){

        int wynik = 0;
        for (int i = 1; i <= 100; i++){
            wynik = wynik + i;
        }
        System.out.println("Suma liczb od 1 do 100 to: " + wynik);
    }
}
