import java.util.Random;

public class Zadanie3_1 {

    public static void main(String[] args) {
        int[] tablica = new int[10];
        Random random = new Random();


        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = random.nextInt(20) - 10;
        }

        for (int i = 0; i < tablica.length; i++) {
            System.out.print(tablica[i] + " ");
        }
        System.out.println();

        int max = tablica[0];
        for (int i = 0; i < tablica.length; i++) {
            if (max < tablica[i]) {
                max = tablica[i];
            }
        }
        System.out.println("Największa liczba to: " + max);

        int min = tablica[0];
        for (int i = 0; i < tablica.length; i++){
            if (min > tablica[i]){
                min = tablica[i];
            }
        }
        System.out.println("Najmniejsza wartość to: " + min);

        int suma = 0;
        for (int i = 0; i < tablica.length; i++){
            suma = suma + tablica[i];
        }
        System.out.println("Suma elementów to " + suma);

        double srednia = (double)suma / tablica.length;
        System.out.println("Średnia z liczb w tablicy to: " + srednia);

        int iloscWiekszych = 0;
        for (int i = 0; i < tablica.length; i++){
            if (tablica[i] > srednia){
                iloscWiekszych = iloscWiekszych + 1;
            }
        }
        System.out.println("Liczb powyżej średniej jest: " + iloscWiekszych);

        int iloscMniejszych = 0;
        for (int i = 0; i < tablica.length; i++){
            if (tablica[i] < srednia){
                iloscMniejszych = iloscWiekszych + 1;
            }
        }
        System.out.println("Liczb poniżej średniej jest: " + iloscMniejszych);

        for (int i = tablica.length-1; i >=  0; i--){
            System.out.print(tablica[i] + " ");
        }
    }
}
