import java.util.Scanner;

public class ZadaniePetle_4 {
    // Napisać program zliczający długość łańcucha znakowego
    // ad. 1 z białymi znakami
    // ad. 2 bez białych znaków
    public static void main (String[] args){
        System.out.println("Napisz cokolwiek");
        Scanner scanner = new Scanner(System.in);
        String napis = scanner.nextLine();

        int dlugoscNapisu = napis.length();
        System.out.println("Długość napisu z białymi znakami to: " + dlugoscNapisu);
        System.out.println();


        String nowyNapis = napis.replace(" ", "");
        int dlugoscNowegoNapisu = nowyNapis.length();
        System.out.println("Długość napisu nie licząc białych znaków to: " + dlugoscNowegoNapisu);


    }
}
