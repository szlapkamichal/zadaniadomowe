import java.util.Scanner;

public class Zadanie2_3 {
    // Wczytać od użytkownika liczbę n (całkowita dodatnia)
    // wyświetlić na ekranie wszystkie potęgi liczby 2 mniejsze od n

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę całkowitą dodatnią");
        int n = scanner.nextInt();
        System.out.println("Wszystkie potęgi liczby 2, mniejsze od podanej liczby to:");

        for (int i = 0; i < n; i++) {
            double potega = Math.pow(2, i);

            if (potega < n) {
                System.out.println(potega);
            }
        }

    }
}
