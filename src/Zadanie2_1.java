import java.util.Scanner;

public class Zadanie2_1 {
    //Pobieramy od użytkownika liczbę całkowitą dodatnią
    //Zwracamy kolejno wszystie liczby nieparzyste mniejsze od tej liczby

    public static void main (String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę");
        int liczba = scanner.nextInt();

        for (int i = 0; i <= liczba ; i++) {
            if (i % 2 != 0) {
                System.out.print(i + " ");
            }
        }
    }
}
