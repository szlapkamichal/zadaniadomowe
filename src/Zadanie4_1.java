import com.sun.org.apache.xpath.internal.functions.FuncStringLength;

import java.util.Scanner;

public class Zadanie4_1 {
    //Poprosić użytkownika o ciag znaków
    //Wypisać ostatni znak
    //Wypisać ile razy ten znak występuje w całym ciagu


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ciag znaków");
        String napis = scanner.next();

        String wszytkoNaMale = napis.toLowerCase();


        char ostatniZnak = wszytkoNaMale.charAt(wszytkoNaMale.length()-1);

        int ileZnakow = wszytkoNaMale.length();

        System.out.println("Ostatni znak to: " + ostatniZnak);

        int suma = 0;
        for (int i = 0; i < ileZnakow; i++){
            if (ostatniZnak == wszytkoNaMale.charAt(i)) {
                suma = suma + 1;
            }
        }
        System.out.println("W podanym ciągu znaków: " + napis + " znak: " + ostatniZnak + " występuje: " + suma );

    }

}
