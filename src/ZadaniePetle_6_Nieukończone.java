public class ZadaniePetle_6_Nieukończone {
    //Napisz program który wyświetli duże litery alfabetu od A do Z
    // następnie do Z do A
    // FOR
    // While
    // Do While

    public static void main(String[] args) {

// While
        System.out.println("Pętla While:");
        char a = 'A';
        while (a <= 'Z') {
            System.out.print(a++ + ", ");
        }
        char z = 'Z';
        while (z >= 'A') {
            System.out.print(z-- + ", ");
        }
// FOR
        System.out.println();
        System.out.println("Pętla For:");
        char b = 'A';
        for (int i = b; i <= 'Z'; i++){
            System.out.print(b++ + ", ");
        }
        char c = 'Z';
        for (int i = 'Z'; i >= 'A'; i--){
            System.out.print(c-- + ", ");
        }
        }
    }


