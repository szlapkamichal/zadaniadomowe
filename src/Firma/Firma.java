package Firma;

public class Firma {
    private static final int MAKSYMALNA_LICZBA_PRACOWNIKOW = 100;
    private String nazwaFirmy;
    private Pracownik[] pracownicy;
    private int liczbaPracownikow; //pokazuje aktualny stan pracowników i indeks w tablicy, gdzie wstawić nowego pracownika

    public Firma (String nazwaFirmy) {
        this.liczbaPracownikow = 0;
        this.nazwaFirmy = nazwaFirmy;
        this.pracownicy = new Pracownik[MAKSYMALNA_LICZBA_PRACOWNIKOW];
    }

    //getter dla pola nazwaFirmy --> bez settera

    public String getNazwaFirmy() {
        return this.nazwaFirmy;
    }

    //getter dla liczbaPracowników -> bez settera
    public int getLiczbaPracownikow() {
        return this.liczbaPracownikow;
    }

    //getter dla pracowników
    public Pracownik[] getPracownicy(){
        return this.pracownicy;
    }

    public boolean dodajPracownika (Pracownik nowyPracownik) {
        if (liczbaPracownikow < MAKSYMALNA_LICZBA_PRACOWNIKOW) {
            pracownicy[liczbaPracownikow++] = nowyPracownik;
            return true;
        }
        return false;
    }

    /**
     *Usuwa pracownika i zwraca referencje do tego pracownika lub zwraca null jeżeli nie znaleziono
     *
     * @param id
     * @return usuwanego pracownikalub nulljeżeli nie znaleziono
     */
    public Pracownik usunPracownika(int id) {
        //todo: Zimplementujkiedy pracownicy będą mieli swoje ID
        Pracownik pracownikDoUsuniecia = null;
        for (int i = 0; i < liczbaPracownikow; i++) {
            if (pracownicy[i].getId() == id) {
                //pracownik znaleziony
                pracownikDoUsuniecia = pracownicy[i];
                pracownicy[i] = null;
                liczbaPracownikow--;
                break;
            }
        }
        //todo: napraw tablicę
        Pracownik[] nowaTablica = new Pracownik[MAKSYMALNA_LICZBA_PRACOWNIKOW];

        //index - wskaźnik w "nowejTAblicy"na pierwsze wolne miejsce - zwiększamy tylko, jeżeli spotkamy pracownika w starej tablicy (a nie null'a)

        int index = 0;
        for (int i = 0; i < pracownicy.length; i++){
            if (pracownicy[i] != null){
                nowaTablica[index] = pracownicy[i];
                index++;
            }
        }
        pracownicy = nowaTablica;
        return pracownikDoUsuniecia;
    }

}


