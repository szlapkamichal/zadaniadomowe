import java.util.Scanner;

public class ZadanieTablice_2 {
    /*
    napisz program wczytujący z klawiatury n liczb całkowitych.
    Liczbę n nalezy pobrać od użytkownika
    Jesli pobrana wartość jest z zakresu 1 - 10, wówczas nalezy pobrać podaną ilosć liczb całkowitych, a następnie wydrukować każdą z liczb podniesioną do kwadratu na ekranie.
    Jesli liczba jest z poza tego przedziału należy zakończyć pracę drukując stosowny komunikat.
     */

    private static Scanner scanner = new Scanner(System.in);

    private static void liczby (int liczba) {
        if (liczba < 10) {
        int[] tablica = new int[liczba];

        for (int i = 0; i < tablica.length; i++) {
            System.out.println("Podaj liczbę nr " + (i + 1));
            tablica[i] = scanner.nextInt();
        }
            for (int i = 0; i < tablica.length; i++) {
                tablica[i] = tablica[i] * tablica[i];
                System.out.println("Kwadrat " + (i + 1) + " liczby to: " + tablica[i]);
            }
        } else {
            System.out.println("Za dużo liczb. Koniec Programu");
        }
    }

    public static void main(String[] args) {
        System.out.println("Podaj ile liczb naturalnych:");
        int liczba = scanner.nextInt();
        liczby(liczba);

    }
}
