import java.util.Scanner;

public class ZadanieFunkcje_10 {
    //napisz program obliczający silnie liczby naturalnej (iteracyjnie)

    // iteracyjnie
    public static int Silnia(int liczba) {
        int wynik = 1;
        for (int i = 1; i <= liczba; i++) {
            wynik = wynik * i;
        }
        return wynik;
    }

    // rekurencyjnie
    public static int SilniaRekurencja(int liczba) {
        if (liczba == 0) {
            return 1;
        } else {
            return (liczba * SilniaRekurencja(liczba - 1));
        }
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę do obliczenia jej silni");
        int liczba = scanner.nextInt();

        System.out.println(Silnia(liczba));
        System.out.println(SilniaRekurencja(liczba));
    }
}
