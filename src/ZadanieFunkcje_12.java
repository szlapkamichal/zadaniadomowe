import java.util.Random;
import java.util.Scanner;

public class ZadanieFunkcje_12 {
    // napisz program który za pomocą instrukcji for znajduje największą i najmnijeszą liczbę ze zbioru n wylosowanych liczb całkowiych od 0 do 99 (w zadaniu n = 5)
    // oraz oblicza średnią ze wszystkich wylosowanych liczb


    public static void losowanie(int liczba) {
        Random random = new Random();
        int[] tablica = new int[liczba];
        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = random.nextInt(99);
            System.out.println(tablica[i]);
        }
        //szukamy MAX z wylosowanych liczb
        int max = tablica[0];
        for (int i = 0; i < tablica.length; i++) {
            if (max < tablica[i]) {
                max = tablica[i];
            }
        }
        System.out.println("Największa wylosowana liczba to: " + max);

        //szukamy MIN z wylosowanych liczb
        int min = tablica[0];
        for (int i = 0; i < tablica.length; i++) {
            if (min > tablica[i]) {
                min = tablica[i];
            }
        }
        System.out.println("Najmniejsza wylosowana liczba to: " + min);

        // obliczamy średnią z wylosowanych liczb
        int suma = 0;
        for (int i = 0; i < tablica.length; i++) {
            suma = suma + tablica[i];
        }
        double srednia = (double) suma / liczba;
        System.out.println("Średnia z wylosowanych liczb to: " + srednia);
    }


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Ile liczb chcesz wylosować? Podaj liczbę");
        int liczba = scanner.nextInt();

        losowanie(liczba);

    }

}