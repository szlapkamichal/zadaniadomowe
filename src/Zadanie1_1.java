import java.util.Scanner;

public class Zadanie1_1 {
    // Program do przeliczania stopni Celcjusza na stopnie Fahrenheit

    public static void main (String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Wpisz st. C");
        double stopnieC = scanner.nextDouble();

        double stopnieF = (1.8 * stopnieC +32);

        System.out.println("St. F: " + stopnieF + "F");


    }
}
