import java.util.Random;

public class ZadanieTablice_1 {
    // Napisz program który w 10-elementowej tablicy jednowymiarowej o nawzie dane umieszcza liczby od 0 do 9

    private static Random random = new Random();

    public static void main(String[] args) {

        int[] tablica = new int[9];

        for (int i = 0; i < tablica.length; i++){
            int liczba = random.nextInt(9);
            tablica[i] = liczba;
            System.out.println(tablica[i]);
        }
    }
}
