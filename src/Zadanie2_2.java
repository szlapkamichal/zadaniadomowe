import java.util.Scanner;

public class Zadanie2_2 {
    /* Pobieramy od uzytkownika A i B
    warunek konieczny A <B
    Program ma zwrócić sume ciągu liczby od A do B
     */

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę A");
        int a = scanner.nextInt();
        System.out.println("Podaj liczbę B");
        int b = scanner.nextInt();

        if (a > b){
            System.out.println("Liczba A musi byc mniejsza od B");
            System.out.println("Uruchom program ponownie");
        } else {

            int suma = 0;
            for (int i = a; i <= b; i++){
                suma = suma + a++;  //jak zapisać to po nazwijmy to EXCELOWEMU??? Bo dlaczego to działa to nie wiem
            }
            System.out.println("Suma z pętli for: " + suma);

            System.out.println(a);


            int wynikWhile = 0;
            while (a <= b){
                System.out.println(a);
                wynikWhile += a++;

            }
            System.out.println("Suma z pętli while: " + wynikWhile);


            int wynikDoWhile = 0;
            do {
                wynikDoWhile += a++;
            } while (a <= b);
            System.out.println("Suma z pętli do while to: " + wynikDoWhile);

        }


    }
}
