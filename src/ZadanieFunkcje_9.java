import sun.nio.cs.ext.MacThai;

import java.util.Scanner;

public class ZadanieFunkcje_9 {
    // wczytać długości trzech odcinków
    //program ma sprawdzić czy  można z nich zbudować trójkąt
    // Jeżeli tak to obliczyć jego pole

    private static Scanner scanner = new Scanner(System.in);


    private static void Trojkat(double bokA, double bokB, double bokC) {
        if (bokA + bokB > bokC && bokB + bokC > bokA && bokC + bokA > bokB) {
            double p = (bokA + bokB + bokC) / 2;
            double pole = Math.sqrt(p * (p - bokA) * (p - bokB) * (p - bokC));
            System.out.println("Można zbudować trójkąt o podanch wymiarach i jego pole równa się: " + pole);
        } else {
            System.out.println("Nie można zbudować trójkąta z podanych odcinków");
        }
    }

    public static void main(String[] args) {

        System.out.println("Podasz 3 liczby. Sprawdzimy czy można z nich zbudować trójkąt i jesli tak to obliczymy jego pole");
        System.out.println("Podaj pierwszy bok:");
        double bokA = scanner.nextDouble();
        System.out.println("Podaj drugi bok:");
        double bokB = scanner.nextDouble();
        System.out.println("Podaj trzecią długość:");
        double bokC = scanner.nextDouble();

        Trojkat(bokA, bokB, bokC);
    }
}
