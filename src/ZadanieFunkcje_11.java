import java.util.Scanner;

public class ZadanieFunkcje_11 {
    // ciąg Fibonacciego

    // iteracyjnie
    public static int Fibo(int liczba) {
        int liczbaA = 0;
        int liczbaB = 1;
        int wynik = 0;
        if (liczba == 0) {
            return 0;
        } else if (liczba == 1) {
            return 1;
        } else {
            for (int i = 0; i <= liczba; i++) {
                wynik = liczbaA;
                liczbaA = liczbaB;
                liczbaB = liczbaB + wynik;
            }
            return wynik;
        }
    }
    //rekurencyjnie
    public static int FiboRekur(int liczba) {
        if (liczba == 0) {
            return 0;
        } else if (liczba == 1) {
            return 1;
        } else {
            return FiboRekur(liczba - 1) + FiboRekur(liczba - 2);
        }
    }

    public static void main(String[] args) {
        System.out.println("Podaj liczbę do obliczenia ciągu Fibanacciego");
        Scanner scanner = new Scanner(System.in);
        int liczba = scanner.nextInt();

        System.out.println("Wynik z iteracji Fibonacciego to: " + Fibo(liczba));
        System.out.println("Wynik z rekurencji Fibanacciego to: " + FiboRekur(liczba));
    }
}
