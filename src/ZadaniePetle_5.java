public class ZadaniePetle_5 {
    //zsumować wszystkie liczby nieparzyste w przedziale od 1 do 100
    // pętla do while

    public static void main (String[] args){

        //parzyste jeśli po % wychodzi 0

        int wynik = 0;
        for(int i = 1; i <=100; i++) {
            if (i % 2 != 0){
                wynik += i;
            }
        }
        System.out.println(wynik);
    }
}
