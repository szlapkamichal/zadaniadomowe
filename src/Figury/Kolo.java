package Figury;

public class Kolo extends Figura {
    private double promien;

    public Kolo(double promien) {
        this.promien = promien;
    }

        //implementacja metody z klasy abstrakcyjnej Figura
    @Override
            public double obliczPole() {
        return Math.PI * (promien * promien);
        }

        // implementacja metody z klasy abstrakcyjnej Figura
    @Override
            public double obliczObwod() {
        return 2 * this.promien * Math.PI;
        }

        // nadpisanie metody toString z klasy Object do zwrócenia tekstowej
    @Override
            public String toString() {
        return "Jestem kołem o promieniu: " + promien;
        }
    }
