package Figury;

import java.util.Scanner;

public class Program {

    private static Scanner scanner = new Scanner(System.in);

    public static Figura figuraDoPoliczenia = null;

    private static void DrukujMenu() {
        System.out.println("Jaka figura: ");
        System.out.println("1. Kwadrat");
        System.out.println("2. Prostokąt");
        System.out.println("3. Koło");
        System.out.println("0. Koniec Programu");
    }

    private static void Kwadrat() {
        System.out.println("Podaj bok");
        double bok = scanner.nextDouble();
        figuraDoPoliczenia = new Kwadrat(bok);
    }

    private static void Prostokat() {
        System.out.println("Podaj bok a");
        double bokA = scanner.nextDouble();
        System.out.println("Podaj bok b");
        double bokB = scanner.nextDouble();
        figuraDoPoliczenia = new Prostokat(bokA, bokB);
    }

    private static void Kolo() {
        System.out.println("Podaj promień");
        double promien = scanner.nextDouble();
        figuraDoPoliczenia = new Kolo(promien);
    }

    public static void main(String[] args) {

        int wybor = -1;

        while (wybor != 0) {
            DrukujMenu();
            wybor = scanner.nextInt();

            switch (wybor) {
                case 1:
                    Kwadrat();
                    break;
                case 2:
                    Prostokat();
                    break;
                case 3:
                    Kolo();
                    break;
                default:
                    System.out.println("błędny wybór");
                    break;

            }

            double pole = figuraDoPoliczenia.obliczPole();
            double obwod = figuraDoPoliczenia.obliczObwod();

            System.out.println(figuraDoPoliczenia);
            System.out.println("Mój obwód to: " + obwod);
            System.out.println("Moje pole to: " + pole);

            System.out.println();
            System.out.println("Liczymy dalej?");
            System.out.println("TAK/NIE");
            String kontynuacja = scanner.next();
            if (kontynuacja.equalsIgnoreCase("NIE")){
                break;
            }

        }
    }
}
