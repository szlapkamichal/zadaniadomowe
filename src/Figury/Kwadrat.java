package Figury;

public class Kwadrat extends Figura {
    private double bokA;

    public Kwadrat (double bokA){
        this.bokA = bokA;
    }

    @Override
    public double obliczPole() {
        return this.bokA * this. bokA;
    }
    @Override
    public double obliczObwod() {
        return this.bokA * 4;
    }

    @Override
    public String toString() {
        return "Jestem kwadratem o boku: " + this.bokA;
    }
}
