package Figury;

public class Prostokat extends Figura {
    private double bokA;
    private double bokB;

    public Prostokat (double bokA, double bokB){
        this.bokA = bokA;
        this.bokB = bokB;
    }

    @Override
    public double obliczPole() {
        return this.bokA * this.bokB;
    }
    @Override
    public double obliczObwod() {
        return (this.bokA * 2) + (this.bokB * 2);
    }
    @Override
    public String toString() {
        return "Jestem prostokątem o bokuA: " + bokA + " oraz bokuB: " + bokB;
    }
}
