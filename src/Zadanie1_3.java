import java.util.Scanner;

public class Zadanie1_3 {
    //Obliczanie BMI

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj swoją wagę");
        double waga = scanner.nextInt();
        System.out.println("Podaj swój wzrost w cm");
        double wzrostUzytkownika = scanner.nextInt();

        double wzrost = wzrostUzytkownika / 100;

        double bmi = waga / (wzrost * wzrost);



        System.out.println("Twoje BMI to: " + bmi);
        if (bmi > 24.9) {
            System.out.println("Nadwaga");
        } else if (bmi < 18.5){
            System.out.println("Niedowaga");
        } else {
            System.out.println("Waga prawidłowa");
        }


    }
}
