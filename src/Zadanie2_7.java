import java.util.Scanner;

public class Zadanie2_7 {
    // narysować prostokąt z np. x-ów

    public static void main(String[] args) {

        System.out.println("Narysujemy prostokąt");
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj jakim znakiem ma być narysowany prostokąt");
        char znakb = '_';
        char znaka = '|';

        System.out.println("Podaj bok a prostokąta");
        int a = scanner.nextInt();
        System.out.println("Podaj bok b prostokąta");
        int b = scanner.nextInt();


        for (int j = 0; j < b; j++) {
            System.out.println();
            for (int i = 0; i < a; i++) {
                if ((j == 0) || (j == b - 1)) {
                    System.out.print(znakb);
                } else if ((i == 0) || (i == a - 1)) {
                    System.out.print(znaka);
                } else {
                    System.out.print(" ");
                }
            }

        }
    }
}