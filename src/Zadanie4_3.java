import java.util.Scanner;

public class Zadanie4_3 {
    // wczytać od użytkownika ciąg znaków
    // sprawdzić czy podany ciąg znaków jest palindromem
    // powinniśmy dać użytkownikowi wybór czy chce wpisać pojedyczny wyraz czy sprawdzić np. zdanie

    public static String stworzWyrazOdwrotnie (String tekst){
        int dlugoszNapisu = tekst.length();
        tekst = tekst.replaceAll(" ", "");
        StringBuilder nowyNapis = new StringBuilder();
        for (int i = tekst.length() - 1; i >= 0; i--) {
            nowyNapis.append(tekst.charAt(i));
        }
        return nowyNapis.toString();
    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ciąg znaków");
        String napis = scanner.nextLine();
        napis = napis.replaceAll(" ", "");
        System.out.println("Czytając ten ciąg znaków otrzymamy: " + stworzWyrazOdwrotnie(napis));

        if (napis.equalsIgnoreCase(stworzWyrazOdwrotnie(napis))){
            System.out.println("Ten ciąg znaków jest palindromem");
        } else {
            System.out.println("Nie jest to palindrom");
        }

    }
}
